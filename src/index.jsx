import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import CounterApp from './reducers/counter.reducer';

const store = createStore(
  CounterApp,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

import {App} from './components/App/App';

if (localStorage.getItem('counter') && localStorage.getItem('counter').split(',').length) {
  const newState = localStorage
    .getItem('counter')
    .split(',')
    .map(value => Number(value));
  store.dispatch({
    type: 'SET_STATE',
    newState
  });
}

store.subscribe(() => localStorage.setItem('counter', store.getState().toArray()));

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);
