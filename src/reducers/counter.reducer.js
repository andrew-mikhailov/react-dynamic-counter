import {List} from 'immutable';

function increase(state, id) {
  return state.set(id, state.get(id) + 1);
}

function decrease(state, id) {
  return state.set(id, state.get(id) - 1);
}

function addCounter(state) {
  return state.push(0);
}

function removeCounter(state) {
  return state.pop();
}

function resetAll(state) {
  return state.map(value => 0)
}

function setState(state, newState) {
  return state.merge(newState);
}

export default function(state = List([]), action) {
  switch (action.type) {
    case 'INCREASE':
      return increase(state, action.id);
    case 'DECREASE':
      return decrease(state, action.id);
    case 'ADD_COUNTER':
      return addCounter(state);
    case 'REMOVE_COUNTER':
      return removeCounter(state);
    case 'RESET_ALL':
      return resetAll(state);
    case 'SET_STATE':
      return setState(state, action.newState);
    default:
      return state;
  }
}
