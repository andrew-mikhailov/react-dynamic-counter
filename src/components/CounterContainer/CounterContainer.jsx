import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './../../helpers/action_creators';
import Counter from "../CounterContainer/Counter/Counter";
import Reseter from "./Reseter/Reseter";

class CounterContainer extends React.Component{
  increment() {
    this.props.addCounter();
  }
  decrement() {
    this.props.removeCounter();
  }
  render() {
    return <div>
      <div>
        <button onClick={() => this.increment()}>Add Counter</button>
        <button onClick={() => this.decrement()}>Remove Counter</button>
        <Reseter/>
      </div>
      <div>
        {this.props.state.map((val, key) => <Counter key={ key } index={key}/>)}
      </div>
    </div>
  }
}

function mapStateToProps(state) {
  return {
    state: state
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(CounterContainer);
