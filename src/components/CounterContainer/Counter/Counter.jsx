import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './../../../helpers/action_creators';

class Counter extends React.Component {
  increment() {
    this.props.increment(this.props.index);
  }
  decrement() {
    this.props.decrement(this.props.index);
  }

  render() {
    const { state, index } = this.props;
    return (
      <div>
        <div key={index}>
          { state.get(index) }
          <div>
            <button onClick={() => this.increment()}>+</button>
            <button onClick={() => this.decrement()}>-</button>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    state: state
  };
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
