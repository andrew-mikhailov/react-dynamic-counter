import React from 'react';
import CounterContainer from "../CounterContainer/CounterContainer";

export class App extends React.Component{
  render() {
    return <div>
      <CounterContainer/>
    </div>
  }
}
